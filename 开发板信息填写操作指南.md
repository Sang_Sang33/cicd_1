# 开发板信息填写操作指南
## 1. 新增开发板
- 进入个人管理中心![图片说明](https://contentcenter-drcn.dbankcdn.com/pub_1/DevEcoSpace_1_900_9/7e/v3/DomGDpMoT9ONC1lvcARDXw/h_D5H7VwStai2VUuKxhyYg.png)

------------

- 进入我的开发板->新增开发板![图片说明](https://contentcenter-drcn.dbankcdn.com/pub_1/DevEcoSpace_1_900_9/e4/v3/svWVl14uSeOnJkIvspVm8A/wze-N--WRT2JlXaa_Tm4kg.png)

------------

- 新增开发板编辑![图片说明](https://contentcenter-drcn.dbankcdn.com/pub_1/DevEcoSpace_1_900_9/86/v3/IUCpNo20TISqPqJaNCbvgA/mFwF5NL1Ta2ifUij-ouEug.png)

------------

###### 	配置项说明：
	- 开发板企业级LOGO(40x40)：必填，只能上传一张照片
	- 开发板图片(300x160)：必填，只能上传一张照片
	- 芯片系统LOGO(40x40)：必填，只能上传一张照片
	- 开发板规格：必填，输入三种规格信息
	- 产品分类：单选，根据开发板案例功能进行分类，例如：智能家居、媒体娱乐、移动办公……
	- OS分类：单选，根据开发板案例复杂度分为轻量系统，小型系统，标准系统
	- OS support：可多选，根据开发板支持的系统版本进行选择，如：1.0.1 LTS、1.1.0 LTS
	- Communication：多选，通过开发板通信协议选择
	- Form Factor：单选，通过开发板影响因子选择
	- 工具：单选，选择开发工具，如：HUAWEI DevEco Studio、HUAWEI DevEco Device Tool

------------

## 2.开发板详情编辑
![图片说明]()

![1653635862692](https://contentcenter-drcn.dbankcdn.com/pub_1/DevEcoSpace_1_900_9/61/v3/kbEnM2LbTRis8rGXqjt47Q/zHJ_SqmUSJ6LWr2LvEz48g.png)

------------

![图片说明]()

![1653636006395](https://contentcenter-drcn.dbankcdn.com/pub_1/DevEcoSpace_1_900_9/2f/v3/i14csypoSU6Fd6BBOshrVw/amAiMHgPRVCDwLF7_oVzYw.png)

------------

###### 	配置项说明：
	- 开发板名称和开发板图片，由上一步上传添加的自动加载
	- 描述信息：添加开发板简短描述信息即可，文本编辑可添加加粗、斜体、标题大小、链接、列表、对齐、表格、引用等
	- 概述：添加开发板概述信息
	- 开源发行版：
	     1.输入HPM官网（https://repo.harmonyos.com/#/cn/solution）找到该开发板进入详情页对应开源发行版名称（必填），例：@bearpi/bearpi_hm_nano  如图所示
	     2. 输入开源发行版版本号（非必填）例：1.0.5  
	     3.输入开源发行版描述信息，输入开发板简短描述即可（非必填）
	- 历史版本（非必填）：输入其他历史版本号即可，例如：1.0.4、1.0.3没有则可以选择删除
	- 功能特性（非必填）：介绍各个组件的功能特性，可以进行添加和删除列表
	- 开发板介绍：介绍开发板的详细情况
	- 开发板案例：可以添加多个开发版案例也可以不填，添加开发板开发出来的实际案例产品信息。
	   1.输入标题，添加开发板案例产品名称 
	   2.介绍信息，添加开发板案例的简短介绍 
	   3.描述信息，添加开发板描述信息

------------

HPM官网（https://repo.harmonyos.com/#/cn/solution）查找开源发行版名称

![1653636163853](https://contentcenter-drcn.dbankcdn.com/pub_1/DevEcoSpace_1_900_9/43/v3/ljwaQfFVQM-ntKtTfUhWMQ/x_tFvu96TIut1b2n8wxC0g.png)



进入HPM对应开发板详情页找到显示的发行版名称

![1653636331165](https://contentcenter-drcn.dbankcdn.com/pub_1/DevEcoSpace_1_900_9/e9/v3/DqvK_cf2SWCqDcYHgtMTNQ/W26TE7Q1TfinBjH1AUSR7A.png)



## 3.开发板适配指导（建议填写）

![图片说明](https://contentcenter-drcn.dbankcdn.com/pub_1/DevEcoSpace_1_900_9/ad/v3/F15H2wxaT0y37xfaXKtL-Q/OLd5nRXHRraJ-FS2MT8o0A.png)

------------

###### 	配置项说明：
- 硬件准备：添加需要准备的硬件工具以及数量，通过+号可以添加多条数据
- 开发环境准备，组件下载&编译准备，驱动设配，产品定制，根据开发板情况添加，通过+号可以添加文本编辑框和上传照片

