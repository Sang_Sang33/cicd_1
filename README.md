# cicd

#### 介绍
OpenHarmony CICD 门禁平台 SIG

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)


#### 开发板进场操作指导

##### 1、CI模板配置方法、CSV填写方法 、操作实例

- [厂商新增openharmony门禁指导](https://gitee.com/openharmony-sig/cicd/blob/master/%E5%8E%82%E5%95%86%E6%96%B0%E5%A2%9Eopenharmony%E9%97%A8%E7%A6%81%E6%8C%87%E5%AF%BCV1.0.md)

#####  2、升级相关要求示范案例

- [鸿蒙流水线自动化升级方案](https://gitee.com/openharmony-sig/cicd/blob/master/%E9%B8%BF%E8%92%99%E6%B5%81%E6%B0%B4%E7%BA%BF%E8%87%AA%E5%8A%A8%E5%8C%96%E5%8D%87%E7%BA%A7%E6%96%B9%E6%A1%88V1.0.md)

#####  3、[XTS用例输出及适配方法]

- [XTS用例输出及适配方法](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/subsystems/subsys-xts-guide.md)

#####  4、[TDD用例输出及适配方法]

https://gitee.com/openharmony/test_developertest
- [TDD用例输出及适配方法](https://gitee.com/openharmony/test_developertest)

#####   5、[发行版制作指导]

- [发行版制作指导](https://gitee.com/openharmony/test_developertest)

#####  6、流水线工具BOX: 1）尺寸：内径 38cm28 cm13 cm 2）为提升利用率，供电线与串口线建议合一

