## SPLE定制发行版操作指导

### 步骤
#### 1.新建分支以及标签
- 在分支管理页面点击左上角按钮添加新的分支，在标签管理页面点击添加标记点为所新增分支的标签

管理入口：
[![](http://image.huawei.com/tiny-lts/v1/images/4c38eed851fa717c23e5d9f94107857f_1728x749.png)](http://image.huawei.com/tiny-lts/v1/images/4c38eed851fa717c23e5d9f94107857f_1728x749.png)

新建分支:
[![](http://image.huawei.com/tiny-lts/v1/images/84c8b7b654105c7d57b2a9e50236f696_1729x688.png)](http://image.huawei.com/tiny-lts/v1/images/84c8b7b654105c7d57b2a9e50236f696_1729x688.png)

新建便签：
[![](http://image.huawei.com/tiny-lts/v1/images/7aaeff9941c84950d22373dfe05d68a9_2368x932.png)](http://image.huawei.com/tiny-lts/v1/images/7aaeff9941c84950d22373dfe05d68a9_2368x932.png)

#### 2.添加发行版
- 在定制发行版页面点击左上角添加按钮，添加新的定制发行版
- 填写发行版的中英文名称，生成特征模型包的名称（名称仅包含数字 ：0-9, 小写字母 ：a-z, 和下划线 ：_），点击OH基线版本后的按钮新增基线版本(步骤1中新建的分支、标签)，选择对应的类型（轻量、小型、标准），填写内部版本号，产品的描述，创建完成会显示创建人、创建时间、发行版等状态

添加发行版：
[![](http://image.huawei.com/tiny-lts/v1/images/07507f8e63f2df60851d4f47b7eec639_1735x715.png)](http://image.huawei.com/tiny-lts/v1/images/07507f8e63f2df60851d4f47b7eec639_1735x715.png)

新增OH基线版本：
[![](http://image.huawei.com/tiny-lts/v1/images/3dced34de51337113d77a546b0189943_1733x736.png)](http://image.huawei.com/tiny-lts/v1/images/3dced34de51337113d77a546b0189943_1733x736.png)

#### 3.添加发行版特性
- 点击装配按钮进入装配界面

[![](http://image.huawei.com/tiny-lts/v1/images/5bc0f6395a8ba23be0e313c2d1047865_2360x481.png)](http://image.huawei.com/tiny-lts/v1/images/5bc0f6395a8ba23be0e313c2d1047865_2360x481.png)

- 点击添加后在弹出框根据特性所在的领域/子系统/部件找到相关特性进行添加

[![](http://image.huawei.com/tiny-lts/v1/images/5934a065df4712b5d48b855a47536e18_2327x1076.png)](http://image.huawei.com/tiny-lts/v1/images/5934a065df4712b5d48b855a47536e18_2327x1076.png)

#### 4.添加部件清单

- 可在该流程页面内单个或批量的对部件的版本（**必填**）以及部件是否为必选（**必填**）进行编辑

批量修改部件版本：
[![](http://image.huawei.com/tiny-lts/v1/images/acecaf2411811b9e3d00210dadaca211_2375x1031.png)](http://image.huawei.com/tiny-lts/v1/images/acecaf2411811b9e3d00210dadaca211_2375x1031.png)

批量修改部件是否必选：
[![](http://image.huawei.com/tiny-lts/v1/images/e1598f1608bd81f9b3e1bc522e8394d4_2392x1033.png)](http://image.huawei.com/tiny-lts/v1/images/e1598f1608bd81f9b3e1bc522e8394d4_2392x1033.png)

修改编辑单个部件：
[![](http://image.huawei.com/tiny-lts/v1/images/d4d3aa93b17889b07d2bda4dca30de4a_2398x1038.png)](http://image.huawei.com/tiny-lts/v1/images/d4d3aa93b17889b07d2bda4dca30de4a_2398x1038.png)

#### 5.配置环境变量

- 可在该流程页面内添加或编辑编译配置，设置hpm命令(**必填**)、输入或生成执行脚本(**必填**)

编译配置：
[![](http://image.huawei.com/tiny-lts/v1/images/c189d287feb1fd5ef48c72fda6ee89c2_2386x1030.png)](http://image.huawei.com/tiny-lts/v1/images/c189d287feb1fd5ef48c72fda6ee89c2_2386x1030.png)

#### 6.检查特性部件清单
- 可在该流程页面内通过发行版特性清单以及发行版部件清单进行检查，如有漏加的或者加多的特性，可以点击上一步回去添加或者删除，部件清单也可通过排序对比检查缺漏，在检查完成点击右下角的完成后发行版添加完毕并在制作发行版页面列表可见

[![](http://image.huawei.com/tiny-lts/v1/images/a667749e9580ef91434332b394ab38d1_2389x1162.png)](http://image.huawei.com/tiny-lts/v1/images/a667749e9580ef91434332b394ab38d1_2389x1162.png)

#### 7.归档及历史版本
- 归档填写版本归档版本号必须按照提示的三段式命名(如：1.0.0、1.0.0-beta)，否则生成的包无法通过hpm

[![](http://image.huawei.com/tiny-lts/v1/images/cf8af443ca85841fb8f025c3ad07aeb9_1669x832.png)](http://image.huawei.com/tiny-lts/v1/images/cf8af443ca85841fb8f025c3ad07aeb9_1669x832.png)

- 归档成功后可以历史版本中下载已归档的所有版本进行使用

[![](http://image.huawei.com/tiny-lts/v1/images/e095a0086edcb4095863fbf2f65a8a23_2383x1132.png)](http://image.huawei.com/tiny-lts/v1/images/e095a0086edcb4095863fbf2f65a8a23_2383x1132.png)

#### 8.发布
- 归档成功后可以对发行版选择发布，发布成功会显示已发布的版本状态以及发布的版本号、发布时间、归档地址等

[![](http://image.huawei.com/tiny-lts/v1/images/922f15325c1e92d019c3ed3439aab7a6_2382x1156.png)](http://image.huawei.com/tiny-lts/v1/images/922f15325c1e92d019c3ed3439aab7a6_2382x1156.png)


